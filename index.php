<?php
	header("Content-Type: application/json");

	switch ( $_SERVER["HTTP_HOST"] ) {
		case "server1.raeworsoftware.com":
			usleep(1000000);
			break;
		case "server2.raeworsoftware.com":
			usleep(2000000);
			break;
	}

	print json_encode(array(
		"status"	=> 200,
		"response"	=> "pong",
		"url"		=> $_SERVER["HTTP_HOST"]
	));